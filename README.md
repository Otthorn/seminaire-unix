# Séminaire UNIX
## Description
Cette semaine on va commencer a aborder doucement Linux. Si tu veux apprendre ce qu'est une console, un shell ou comment marche les fichiers sous Linux alors tu es bienvenue. On va aussi parler de ssh et de irc, un protocole pour envoyer des messages minimaliste pour qu'on puisse garder le contact après les séminaires \o/

Introduction à Unix, GNU/Linux, Debian et tutti quanti + shell/ssh/cli

Présentation le 28 Janvier 2021

## Compilation

Just run

```
pdflatex main.tex
```
